package com.example.capstoneproject.util

import androidx.appcompat.app.AppCompatDelegate

object Constants {
    const val DURATION: Long = 4000
    const val LIGHT_THEME = AppCompatDelegate.MODE_NIGHT_NO
    const val DARK_THEME = AppCompatDelegate.MODE_NIGHT_YES
    const val FOLLOW_SYSTEM_THEME = AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
    const val SETTINGS_SHARED_PREFS = "settings_shared_prefs"
    const val THEME_MODE = "themeMode"
    const val QUERY_PAGE_SIZE = 20
}