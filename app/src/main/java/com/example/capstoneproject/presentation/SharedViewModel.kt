package com.example.capstoneproject.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.capstoneproject.R
import com.example.capstoneproject.domain.models.ArticleEntity
import com.example.capstoneproject.domain.usecase.DeleteSavedNewsUseCase
import com.example.capstoneproject.domain.usecase.GetBreakingNewsUseCase
import com.example.capstoneproject.domain.usecase.GetSavedNewsUseCase
import com.example.capstoneproject.domain.usecase.SaveFavoriteNewsUseCase
import com.example.capstoneproject.presentation.adapters.DelegateAdapterItem
import com.example.capstoneproject.presentation.models.Article
import com.example.capstoneproject.presentation.models.Header
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SharedViewModel(
    private val getBreakingNewsUseCase: GetBreakingNewsUseCase,
    private val saveFavoriteNewsUseCase: SaveFavoriteNewsUseCase,
    private val getSavedNewsUseCase: GetSavedNewsUseCase,
    private val deleteSavedNewsUseCase: DeleteSavedNewsUseCase
) : ViewModel() {

    private val _recyclerViewItemsLiveData = MutableLiveData<List<DelegateAdapterItem>>()
    val recyclerViewItemsLiveData: LiveData<List<DelegateAdapterItem>> = _recyclerViewItemsLiveData

    private val _savedArticlesLiveData = MutableLiveData<List<DelegateAdapterItem>>()
    val savedArticlesLiveData: LiveData<List<DelegateAdapterItem>> = _savedArticlesLiveData

    private val _selectedArticle = MutableLiveData<Article>()
    val selectedArticle: LiveData<Article> = _selectedArticle


    private var currentPage = 1

    init {
        getBreakingNews("us", currentPage)
    }

    fun postSelectedArticle(article: Article) {
        _selectedArticle.value = article
    }

    fun saveFavoriteArticle(article: Article) = viewModelScope.launch {
        saveFavoriteNewsUseCase.invoke(
            ArticleEntity(
                id = article.id,
                description = article.description,
                title = article.title,
                url = article.url,
                urlToImage = article.urlToImage
            )
        )
        getSavedNews()
    }

    fun deleteFavoriteArticle(article: Article) = viewModelScope.launch {
        deleteSavedNewsUseCase.invoke(
            ArticleEntity(
                id = article.id,
                description = article.description,
                title = article.title,
                url = article.url,
                urlToImage = article.urlToImage
            )
        )
        getSavedNews()
    }

    fun getSavedNews() {
        viewModelScope.launch(Dispatchers.IO) {
            getSavedNewsUseCase().collect { articles ->
                val list = mutableListOf<DelegateAdapterItem>()
                val articleItems = articles.map { article ->
                    Article(
                        id = article.id,
                        description = article.description,
                        title = article.title,
                        url = article.url,
                        urlToImage = article.urlToImage
                    )
                }
                list.addAll(articleItems)
                _savedArticlesLiveData.postValue(list)
            }
        }
    }

    fun getBreakingNews(countryCode: String, page: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            getBreakingNewsUseCase.invoke(countryCode, page)
                .collect { articles ->
                    val list = mutableListOf<DelegateAdapterItem>()

                    val header = Header(id = null, headerImage = R.drawable.news_img)
                    list.add(header)

                    val articleItems = articles.map { article ->
                        Article(
                            id = article.id,
                            description = article.description,
                            title = article.title,
                            url = article.url,
                            urlToImage = article.urlToImage
                        )
                    }
                    list.addAll(articleItems)
                    _recyclerViewItemsLiveData.postValue(list)
                }
        }
    }

    fun loadNextPage(countryCode: String) {
        currentPage++
        getBreakingNews(countryCode, currentPage)
    }
}

