package com.example.capstoneproject.presentation.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.AbsListView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.capstoneproject.R
import com.example.capstoneproject.app.App
import com.example.capstoneproject.databinding.FragmentBreakingNewsBinding
import com.example.capstoneproject.presentation.MainActivity
import com.example.capstoneproject.presentation.SharedViewModel
import com.example.capstoneproject.presentation.ViewModelProviderFactory
import com.example.capstoneproject.presentation.adapters.ArticleDelegateAdapter
import com.example.capstoneproject.presentation.adapters.CompositeAdapter
import com.example.capstoneproject.presentation.adapters.HeaderDelegateAdapter
import com.example.capstoneproject.util.Constants.QUERY_PAGE_SIZE
import javax.inject.Inject


class BreakingNewsFragment : Fragment(R.layout.fragment_breaking_news) {

    private val binding by viewBinding(FragmentBreakingNewsBinding::bind)
    private val sharedViewModel: SharedViewModel by viewModels(
        { activity as MainActivity },
        factoryProducer = { viewModelProviderFactory })

    private val compositeAdapter by lazy {
        CompositeAdapter.Builder()
            .add(HeaderDelegateAdapter())
            .add(ArticleDelegateAdapter(clickListener = { article ->
                sharedViewModel.postSelectedArticle(article)
                findNavController().navigate(
                    R.id.action_breakingNewsFragment2_to_articleFragment
                )
            }))
            .build()
    }

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as App)
            .appComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        setObservers()
        setupScrollListener()
    }

    private fun setUpRecyclerView() {
        binding.rvBreakingNews.adapter = compositeAdapter
    }

    private fun setObservers() {
        sharedViewModel.recyclerViewItemsLiveData.observe(viewLifecycleOwner) {
            compositeAdapter.submitList(it)
        }
    }

    private var isLastPage = false
    private var isLoading = false
    private var isScrolling = false

    private fun setupScrollListener() {
        binding.rvBreakingNews.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount

                val isNotLoadingAndNotLastPage = !isLoading && !isLastPage
                val isAtLastItem = firstVisibleItemPosition + visibleItemCount >= totalItemCount
                val isNotAtBeginning = firstVisibleItemPosition >= 0
                val isTotalMoreThanVisible = totalItemCount >= QUERY_PAGE_SIZE
                val shouldPaginate =
                    isNotLoadingAndNotLastPage && isAtLastItem && isNotAtBeginning &&
                            isTotalMoreThanVisible && isScrolling

                if (shouldPaginate) {
                    sharedViewModel.loadNextPage("us")
                    isScrolling = false
                }
            }
        })
    }
}