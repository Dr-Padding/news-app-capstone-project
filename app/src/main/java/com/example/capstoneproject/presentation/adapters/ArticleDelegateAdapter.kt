package com.example.capstoneproject.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.capstoneproject.R
import com.example.capstoneproject.databinding.ItemArticlePreviewBinding
import com.example.capstoneproject.presentation.models.Article

class ArticleDelegateAdapter(
    private val clickListener: (Article) -> Unit
) :
    DelegateAdapter<Article, ArticleDelegateAdapter.ArticleViewHolder>(Article::class.java) {


    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val binding =
            ItemArticlePreviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ArticleViewHolder(binding)
    }

    override fun bindViewHolder(
        model: Article,
        viewHolder: ArticleViewHolder,
        payloads: List<DelegateAdapterItem.Payloadable>
    ) {
        when (val payload = payloads.firstOrNull() as? Article.ChangePayload) {
            is Article.ChangePayload.TitleChanged ->
                viewHolder.bindTitle(payload.title)

            is Article.ChangePayload.DescriptionChanged ->
                viewHolder.bindDescription(payload.description)

            else -> viewHolder.bind(model)
        }
    }

    inner class ArticleViewHolder(private val binding: ItemArticlePreviewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Article) {
            Glide.with(binding.root)
                .load(item.urlToImage)
                .apply(RequestOptions.bitmapTransform(RoundedCorners(25)))
                .placeholder(R.drawable.no_photo)
                .error(R.drawable.no_photo)
                .into(binding.ivArticleImage)
            binding.tvTitle.text = item.title
            binding.tvDescription.text = item.description

            binding.root.setOnClickListener {
                clickListener(item)
            }

        }

        fun bindTitle(title: String?) {
            binding.tvTitle.text = title
        }

        fun bindDescription(description: String?) {
            binding.tvDescription.text = description
        }
    }
}
