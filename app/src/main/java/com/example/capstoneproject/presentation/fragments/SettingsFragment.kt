package com.example.capstoneproject.presentation.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.capstoneproject.R
import com.example.capstoneproject.databinding.FragmentBreakingNewsBinding
import com.example.capstoneproject.databinding.FragmentSettingsBinding
import com.example.capstoneproject.presentation.SharedViewModel
import com.example.capstoneproject.util.Constants
import com.example.capstoneproject.util.Constants.DARK_THEME
import com.example.capstoneproject.util.Constants.FOLLOW_SYSTEM_THEME
import com.example.capstoneproject.util.Constants.LIGHT_THEME
import com.example.capstoneproject.util.Constants.SETTINGS_SHARED_PREFS
import com.example.capstoneproject.util.Constants.THEME_MODE
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject


class SettingsFragment : Fragment(R.layout.fragment_settings) {

    private val binding by viewBinding(FragmentSettingsBinding::bind)
    private lateinit var dialog: AlertDialog
    private lateinit var sharedPreferences: SharedPreferences

    private val editor: SharedPreferences.Editor by lazy {
        sharedPreferences.edit()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedPreferences = requireActivity().getSharedPreferences(
            SETTINGS_SHARED_PREFS,
            Context.MODE_PRIVATE
        )

        binding.clThemeSettings.setOnClickListener {
            showThemeDialog()
        }
        binding.userThemeChoice.text = getThemeChoiceText(getThemePreference())
    }

    private fun showThemeDialog() {
        val currentTheme = getThemePreference()

        val themeOptions = arrayOf<CharSequence>(
            getString(R.string.dark_theme_mode_off),
            getString(R.string.dark_theme_mode_on),
            getString(R.string.dark_theme_mode_follow_system_settings)
        )

        val selectedIndex = getThemeSelectionIndex(currentTheme)

        dialog = AlertDialog.Builder(requireContext())
            .setTitle(R.string.alert_dialog_title)
            .setSingleChoiceItems(themeOptions, selectedIndex) { _, which ->
                when (which) {
                    0 -> setThemePreference(LIGHT_THEME)
                    1 -> setThemePreference(DARK_THEME)
                    2 -> setThemePreference(FOLLOW_SYSTEM_THEME)
                }
                dialog.dismiss()
            }
            .setNegativeButton(R.string.CANCEL) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
        dialog.show()
    }

    private fun setThemePreference(theme: Int) {
        editor.putInt(THEME_MODE, theme).apply()
        binding.userThemeChoice.text = getThemeChoiceText(theme)
        AppCompatDelegate.setDefaultNightMode(theme)
    }

    private fun getThemePreference(): Int {
        return sharedPreferences.getInt(THEME_MODE, FOLLOW_SYSTEM_THEME)
    }

    private fun getThemeChoiceText(theme: Int): String {
        return when (theme) {
            LIGHT_THEME -> getString(R.string.dark_theme_mode_off)
            DARK_THEME -> getString(R.string.dark_theme_mode_on)
            else -> getString(R.string.dark_theme_mode_follow_system_settings)
        }
    }

    private fun getThemeSelectionIndex(theme: Int): Int {
        return when (theme) {
            LIGHT_THEME -> 0
            DARK_THEME -> 1
            else -> 2
        }
    }
}




