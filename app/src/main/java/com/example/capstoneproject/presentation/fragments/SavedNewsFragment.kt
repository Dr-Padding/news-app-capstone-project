package com.example.capstoneproject.presentation.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.capstoneproject.R
import com.example.capstoneproject.app.App
import com.example.capstoneproject.databinding.FragmentBreakingNewsBinding
import com.example.capstoneproject.databinding.FragmentSavedNewsBinding
import com.example.capstoneproject.presentation.MainActivity
import com.example.capstoneproject.presentation.SharedViewModel
import com.example.capstoneproject.presentation.ViewModelProviderFactory
import com.example.capstoneproject.presentation.adapters.ArticleDelegateAdapter
import com.example.capstoneproject.presentation.adapters.CompositeAdapter
import com.example.capstoneproject.presentation.adapters.HeaderDelegateAdapter
import com.example.capstoneproject.presentation.models.Article
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

class SavedNewsFragment : Fragment(R.layout.fragment_saved_news) {

    private val binding by viewBinding(FragmentSavedNewsBinding::bind)
    private val sharedViewModel: SharedViewModel by viewModels(
        { activity as MainActivity },
        factoryProducer = { viewModelProviderFactory })

    private val compositeAdapter by lazy {
        CompositeAdapter.Builder()
            .add(ArticleDelegateAdapter(clickListener = { article ->
                sharedViewModel.postSelectedArticle(article)
                findNavController().navigate(
                    R.id.action_savedNewsFragment2_to_articleFragment
                )
            }))
            .build()
    }

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as App)
            .appComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel.getSavedNews()
        setUpRecyclerView()
        setObservers()
    }

    private fun setUpRecyclerView() {
        binding.rvSavedNews.adapter = compositeAdapter

        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val item =
                    compositeAdapter.getItemAtPosition(position)

                if (item is Article) {
                    sharedViewModel.deleteFavoriteArticle(item)


                    view?.let {
                        Snackbar.make(
                            it,
                            R.string.successfully_deleted_article,
                            Snackbar.LENGTH_LONG
                        ).apply {
                            setAction(R.string.undo) {
                                sharedViewModel.saveFavoriteArticle(item)
                            }
                            show()
                        }
                    }
                }
            }
        }

        ItemTouchHelper(itemTouchHelperCallback).apply {
            attachToRecyclerView(binding.rvSavedNews)
        }
    }

    private fun setObservers() {
        sharedViewModel.savedArticlesLiveData.observe(viewLifecycleOwner) {
            compositeAdapter.submitList(it)
        }
    }
}