package com.example.capstoneproject.presentation.models

import com.example.capstoneproject.presentation.adapters.DelegateAdapterItem


data class Article(
    var id: Int? = null,
    val description: String,
    val title: String,
    val url: String,
    val urlToImage: String
) : DelegateAdapterItem {

    override fun id(): Int? = id

    override fun content(): Any = ArticleContent(title, description)

    override fun payload(other: Any): DelegateAdapterItem.Payloadable {
        if (other is Article) {
            if (title != other.title) {
                return ChangePayload.TitleChanged(other.title)
            }

            if (description != other.description) {
                return ChangePayload.DescriptionChanged(other.description)
            }
        }
        return DelegateAdapterItem.Payloadable.None
    }

    inner class ArticleContent(val title: String?, val description: String?) {

        override fun equals(other: Any?): Boolean {
            if (other is ArticleContent) {
                return title == other.title && description == other.description
            }
            return false
        }

        override fun hashCode(): Int {
            var result = title.hashCode()
            result = 31 * result + description.hashCode()
            return result
        }

    }

    sealed class ChangePayload : DelegateAdapterItem.Payloadable {
        data class TitleChanged(val title: String?) : ChangePayload()
        data class DescriptionChanged(val description: String?) : ChangePayload()
    }
}

