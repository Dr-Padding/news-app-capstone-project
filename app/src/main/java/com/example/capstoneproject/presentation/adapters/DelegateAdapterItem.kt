package com.example.capstoneproject.presentation.adapters

interface DelegateAdapterItem {
    fun id(): Int?

    fun content(): Any

    fun payload(other: Any): Payloadable = Payloadable.None

    /**
     * Simple marker interface for payloads
     */
    interface Payloadable {
        object None : Payloadable
    }
}