package com.example.capstoneproject.presentation.models

data class Source(
    val id: Any?,
    val name: String?
)