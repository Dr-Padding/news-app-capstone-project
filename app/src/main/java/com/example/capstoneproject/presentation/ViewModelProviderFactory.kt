package com.example.capstoneproject.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.capstoneproject.domain.usecase.DeleteSavedNewsUseCase
import com.example.capstoneproject.domain.usecase.GetBreakingNewsUseCase
import com.example.capstoneproject.domain.usecase.GetSavedNewsUseCase
import com.example.capstoneproject.domain.usecase.SaveFavoriteNewsUseCase

class ViewModelProviderFactory(
    val getBreakingNewsUseCase: GetBreakingNewsUseCase,
    val saveFavoriteNewsUseCase: SaveFavoriteNewsUseCase,
    val getSavedNewsUseCase: GetSavedNewsUseCase,
    val deleteSavedNewsUseCase: DeleteSavedNewsUseCase
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SharedViewModel(
            getBreakingNewsUseCase = getBreakingNewsUseCase,
            saveFavoriteNewsUseCase = saveFavoriteNewsUseCase,
            getSavedNewsUseCase = getSavedNewsUseCase,
            deleteSavedNewsUseCase = deleteSavedNewsUseCase
        ) as T
    }

}