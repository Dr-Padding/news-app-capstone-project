package com.example.capstoneproject.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.capstoneproject.databinding.ItemHeaderPreviewBinding
import com.example.capstoneproject.presentation.models.Header

class HeaderDelegateAdapter : DelegateAdapter<Header, HeaderDelegateAdapter.HeaderViewHolder>(
    Header::class.java
) {
    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val binding =
            ItemHeaderPreviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HeaderViewHolder(binding)
    }

    override fun bindViewHolder(
        model: Header,
        viewHolder: HeaderViewHolder,
        payloads: List<DelegateAdapterItem.Payloadable>
    ) {
        when (val payload = payloads.firstOrNull() as? Header.ChangePayload) {
            is Header.ChangePayload.HeaderImageChanged ->
                viewHolder.bindHeaderImage(payload.headerImage)

            else -> viewHolder.bind(model)
        }
    }

    inner class HeaderViewHolder(private val binding: ItemHeaderPreviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Header) {
            binding.ivHeader.setImageResource(item.headerImage)
        }

        fun bindHeaderImage(headerImage: Int) {
            binding.ivHeader.setImageResource(headerImage)
        }
    }
}