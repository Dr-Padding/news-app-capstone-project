package com.example.capstoneproject.presentation.models

import com.example.capstoneproject.presentation.adapters.DelegateAdapterItem

data class Header(
    val id: Int? = null,
    var headerImage: Int
) : DelegateAdapterItem {

    override fun id(): Int? = id

    override fun content(): Any = HeaderContent(headerImage)

    override fun payload(other: Any): DelegateAdapterItem.Payloadable {
        if (other is Header) {
            if (headerImage != other.headerImage) {
                return ChangePayload.HeaderImageChanged(other.headerImage)
            }
        }
        return DelegateAdapterItem.Payloadable.None
    }

    inner class HeaderContent(val headerImage: Int) {

        override fun equals(other: Any?): Boolean {
            if (other is HeaderContent) {
                return headerImage == other.headerImage
            }
            return false
        }

        override fun hashCode(): Int {
            var result = headerImage.hashCode()
            result = 31 * result + headerImage.hashCode()
            return result
        }

    }

    sealed class ChangePayload : DelegateAdapterItem.Payloadable {
        data class HeaderImageChanged(val headerImage: Int) : ChangePayload()
    }
}