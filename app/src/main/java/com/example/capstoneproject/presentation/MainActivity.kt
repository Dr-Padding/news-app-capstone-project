package com.example.capstoneproject.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.capstoneproject.R
import com.example.capstoneproject.databinding.ActivityMainBinding
import com.example.capstoneproject.util.Constants
import com.example.capstoneproject.util.Constants.SETTINGS_SHARED_PREFS
import com.example.capstoneproject.util.Constants.THEME_MODE

class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::bind)

    override fun onCreate(savedInstanceState: Bundle?) {
        val defaultThemeMode = getSharedPreferences(SETTINGS_SHARED_PREFS, MODE_PRIVATE)
            .getInt(THEME_MODE, Constants.FOLLOW_SYSTEM_THEME)
        AppCompatDelegate.setDefaultNightMode(defaultThemeMode)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        val navController = navHostFragment.navController
        binding.bottomNavigationView.setupWithNavController(navController)
    }
}