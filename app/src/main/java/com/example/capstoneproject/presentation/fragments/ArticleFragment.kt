package com.example.capstoneproject.presentation.fragments


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.capstoneproject.R
import com.example.capstoneproject.app.App
import com.example.capstoneproject.databinding.FragmentArticleBinding
import com.example.capstoneproject.presentation.MainActivity
import com.example.capstoneproject.presentation.SharedViewModel
import com.example.capstoneproject.presentation.ViewModelProviderFactory
import com.example.capstoneproject.presentation.models.Article
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject


class ArticleFragment : Fragment(R.layout.fragment_article) {

    private val binding by viewBinding(FragmentArticleBinding::bind)
    private val sharedViewModel: SharedViewModel by viewModels(
        { activity as MainActivity },
        factoryProducer = { viewModelProviderFactory })
    lateinit var breakingNewsArticle: Article

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as App)
            .appComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setObservers()
    }

    private fun initViews() {
        binding.apply {
            fab.setOnClickListener {
                sharedViewModel.saveFavoriteArticle(breakingNewsArticle)
                view?.let { it1 ->
                    Snackbar.make(it1, R.string.article_saved, Snackbar.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun setObservers() {
        sharedViewModel.selectedArticle.observe(viewLifecycleOwner) { article ->
            breakingNewsArticle = article
            binding.webView.apply {
                webViewClient = WebViewClient()
                if (article != null) {
                    loadUrl(article.url)
                }
            }
        }
    }
}
