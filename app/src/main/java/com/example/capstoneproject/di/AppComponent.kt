package com.example.capstoneproject.di

import com.example.capstoneproject.presentation.fragments.ArticleFragment
import com.example.capstoneproject.presentation.fragments.BreakingNewsFragment
import com.example.capstoneproject.presentation.fragments.SavedNewsFragment
import dagger.Component

@Component(modules = [AppModule::class, DomainModule::class, DataModule::class])
interface AppComponent {
    fun inject(breakingNewsFragment: BreakingNewsFragment)
    fun inject(articleFragment: ArticleFragment)
    fun inject(savedNewsFragment: SavedNewsFragment)
}