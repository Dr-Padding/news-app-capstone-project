package com.example.capstoneproject.di

import android.content.Context
import com.example.capstoneproject.domain.usecase.DeleteSavedNewsUseCase
import com.example.capstoneproject.domain.usecase.GetBreakingNewsUseCase
import com.example.capstoneproject.domain.usecase.GetSavedNewsUseCase
import com.example.capstoneproject.domain.usecase.SaveFavoriteNewsUseCase
import com.example.capstoneproject.presentation.ViewModelProviderFactory
import dagger.Module
import dagger.Provides

@Module
class AppModule(val context: Context) {

    @Provides
    fun provideContext(): Context {
        return context
    }

    @Provides
    fun provideViewModelProviderFactory(
        getBreakingNewsUseCase: GetBreakingNewsUseCase,
        saveFavoriteNewsUseCase: SaveFavoriteNewsUseCase,
        getSavedNewsUseCase: GetSavedNewsUseCase,
        deleteSavedNewsUseCase: DeleteSavedNewsUseCase
    ): ViewModelProviderFactory {
        return ViewModelProviderFactory(
            getBreakingNewsUseCase = getBreakingNewsUseCase,
            saveFavoriteNewsUseCase = saveFavoriteNewsUseCase,
            getSavedNewsUseCase = getSavedNewsUseCase,
            deleteSavedNewsUseCase = deleteSavedNewsUseCase
        )
    }

}