package com.example.capstoneproject.di

import com.example.capstoneproject.domain.repository.NewsRepository
import com.example.capstoneproject.domain.usecase.DeleteSavedNewsUseCase
import com.example.capstoneproject.domain.usecase.GetBreakingNewsUseCase
import com.example.capstoneproject.domain.usecase.GetSavedNewsUseCase
import com.example.capstoneproject.domain.usecase.SaveFavoriteNewsUseCase
import dagger.Module
import dagger.Provides

@Module
class DomainModule {
    @Provides
    fun provideGetBreakingNewsUseCase(newsRepository: NewsRepository): GetBreakingNewsUseCase {
        return GetBreakingNewsUseCase(newsRepository = newsRepository)
    }

    @Provides
    fun provideSaveFavoriteNewsUseCase(newsRepository: NewsRepository): SaveFavoriteNewsUseCase {
        return SaveFavoriteNewsUseCase(newsRepository = newsRepository)
    }

    @Provides
    fun provideGetSavedNewsUseCase(newsRepository: NewsRepository): GetSavedNewsUseCase {
        return GetSavedNewsUseCase(newsRepository = newsRepository)
    }

    @Provides
    fun provideDeleteSavedNewsUseCase(newsRepository: NewsRepository): DeleteSavedNewsUseCase {
        return DeleteSavedNewsUseCase(newsRepository = newsRepository)
    }
}