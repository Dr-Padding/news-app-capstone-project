package com.example.capstoneproject.di

import android.content.Context
import androidx.room.Room
import com.example.capstoneproject.data.mappers.NewsResponseMapper
import com.example.capstoneproject.data.repository.NewsRepositoryImpl
import com.example.capstoneproject.data.database.ArticleDatabase
import com.example.capstoneproject.data.mappers.ArticleMapper
import com.example.capstoneproject.domain.repository.NewsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {
    @Provides
    fun provideNewsRepository(
        articleDatabase: ArticleDatabase,
        newsResponseMapper: NewsResponseMapper,
        articleMapper: ArticleMapper
    ): NewsRepository {
        return NewsRepositoryImpl(
            articleDatabase = articleDatabase,
            newsResponseMapper = newsResponseMapper,
            articleMapper = articleMapper
        )
    }

    @Provides
    fun provideArticleDatabase(context: Context): ArticleDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            ArticleDatabase::class.java,
            "article_db.db"
        ).build()
    }
}
