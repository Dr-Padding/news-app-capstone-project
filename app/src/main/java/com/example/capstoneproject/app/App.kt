package com.example.capstoneproject.app

import android.app.Application
import com.example.capstoneproject.di.AppComponent
import com.example.capstoneproject.di.AppModule
import com.example.capstoneproject.di.DaggerAppComponent

class App : Application() {

    lateinit var appComponent: AppComponent
    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent
            .builder()
            .appModule(AppModule(context = this))
            .build()
    }
}