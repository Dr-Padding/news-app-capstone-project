package com.example.capstoneproject.presentation


import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.example.capstoneproject.R
import org.junit.Rule
import org.junit.Test

class MainActivityTest {
    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun clickButtonBreakingNews() {
        Espresso.onView(ViewMatchers.withId(R.id.breakingNewsFragment2))
            .perform(ViewActions.click()).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun clickButtonSettings() {
        Espresso.onView(ViewMatchers.withId(R.id.settingsFragment))
            .perform(ViewActions.click()).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}