package com.example.capstoneproject.domain.usecase

import com.example.capstoneproject.domain.models.ArticleEntity
import com.example.capstoneproject.domain.models.NewsResponseDomain
import com.example.capstoneproject.domain.repository.NewsRepository
import com.example.capstoneproject.domain.repository.Result
import kotlinx.coroutines.flow.Flow

class SaveFavoriteNewsUseCase(
    private val newsRepository: NewsRepository
) {
    suspend operator fun invoke(
        articleEntity: ArticleEntity
    ) {
        newsRepository.saveFavoriteArticle(articleEntity)
    }
}

