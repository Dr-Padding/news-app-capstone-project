package com.example.capstoneproject.domain.repository

import com.example.capstoneproject.domain.models.ArticleEntity
import kotlinx.coroutines.flow.Flow

interface NewsRepository {
    suspend fun observeAllArticles(countryCode: String, pageNumber: Int): Flow<List<ArticleEntity>>
    suspend fun getSavedArticles(): Flow<List<ArticleEntity>>
    suspend fun saveFavoriteArticle(article: ArticleEntity)
    suspend fun deleteArticle(article: ArticleEntity)
}
