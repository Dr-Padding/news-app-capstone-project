package com.example.capstoneproject.domain.models


data class ArticleEntity(
    var id: Int? = null,
    val description: String,
    val title: String,
    val url: String,
    val urlToImage: String
)
