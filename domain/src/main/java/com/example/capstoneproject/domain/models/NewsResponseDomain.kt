package com.example.capstoneproject.domain.models

data class NewsResponseDomain(
    val articles: List<ArticleEntity>,
    val status: String,
    val totalResults: Int
)