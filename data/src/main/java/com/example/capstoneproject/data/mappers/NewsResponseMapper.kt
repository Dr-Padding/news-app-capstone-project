package com.example.capstoneproject.data.mappers

import com.example.capstoneproject.data.models.remote.NewsResponseData
import com.example.capstoneproject.domain.models.NewsResponseDomain
import javax.inject.Inject

class NewsResponseMapper @Inject constructor(private val articleMapper: ArticleMapper) {
    fun mapToDomain(newsResponseData: NewsResponseData): NewsResponseDomain {
        val domainArticles =
            newsResponseData.articles.map { articleMapper.mapFromData(it) }.toMutableList()
        return NewsResponseDomain(
            articles = domainArticles,
            status = newsResponseData.status,
            totalResults = newsResponseData.totalResults
        )
    }
}

