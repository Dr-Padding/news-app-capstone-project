package com.example.capstoneproject.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.capstoneproject.data.models.db.ArticleData
import kotlinx.coroutines.flow.Flow


@Dao
interface ArticleDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertAllArticles(article: List<ArticleData>)

    @Query("SELECT * FROM articles")
    fun getAllArticles(): Flow<List<ArticleData>>
}