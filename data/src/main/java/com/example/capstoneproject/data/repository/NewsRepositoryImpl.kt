package com.example.capstoneproject.data.repository

import com.example.capstoneproject.data.api.RetrofitInstance
import com.example.capstoneproject.data.database.ArticleDatabase
import com.example.capstoneproject.data.mappers.ArticleMapper
import com.example.capstoneproject.data.mappers.NewsResponseMapper
import com.example.capstoneproject.data.models.db.ArticleData
import com.example.capstoneproject.domain.models.ArticleEntity
import com.example.capstoneproject.domain.models.NewsResponseDomain
import com.example.capstoneproject.domain.repository.NewsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class NewsRepositoryImpl(
    val articleDatabase: ArticleDatabase,
    private val newsResponseMapper: NewsResponseMapper,
    private val articleMapper: ArticleMapper
) : NewsRepository {
    override suspend fun observeAllArticles(
        countryCode: String,
        pageNumber: Int
    ): Flow<List<ArticleEntity>> {
        try {
            val response = RetrofitInstance.api.getBreakingNews(countryCode, pageNumber)
            val dataNewsResponse = response.body()
            if (response.isSuccessful && dataNewsResponse != null) {
                val domainNewsResponse = newsResponseMapper.mapToDomain(dataNewsResponse)
                saveArticlesToDatabase(dataNewsResponse.articles)
                Result.success(domainNewsResponse)
            } else {
                Result.failure(Exception("Failed to get breaking news"))
            }
        } catch (e: Exception) {
            Result.failure(Exception(e.message ?: "An error occurred"))
        }

        return articleDatabase.getArticleDao().getAllArticles().map { articleDataList ->
            articleDataList.map { articleData ->
                ArticleEntity(
                    id = articleData.id,
                    description = articleData.description.orEmpty(),
                    title = articleData.title.orEmpty(),
                    url = articleData.url.orEmpty(),
                    urlToImage = articleData.urlToImage.orEmpty()
                )
            }
        }
    }

    override suspend fun getSavedArticles(): Flow<List<ArticleEntity>> {
        return articleDatabase.getFavoriteArticleDao().getSavedArticles()
            .map { articleFavoriteList ->
                articleFavoriteList.map { articleFavorite ->
                    ArticleEntity(
                        id = articleFavorite.id,
                        description = articleFavorite.description.orEmpty(),
                        title = articleFavorite.title.orEmpty(),
                        url = articleFavorite.url.orEmpty(),
                        urlToImage = articleFavorite.urlToImage.orEmpty()
                    )
                }
            }
    }

    override suspend fun saveFavoriteArticle(article: ArticleEntity) {
        val favoriteArticle = articleMapper.mapToFavoriteArticleData(article)
        articleDatabase.getFavoriteArticleDao().insertArticle(favoriteArticle)
    }

    override suspend fun deleteArticle(article: ArticleEntity) {
        val favoriteArticle = articleMapper.mapToFavoriteArticleData(article)
        articleDatabase.getFavoriteArticleDao().deleteArticle(favoriteArticle)
    }

    private fun saveArticlesToDatabase(articles: List<ArticleData>) {
        articleDatabase.getArticleDao().upsertAllArticles(articles)
    }
}
