package com.example.capstoneproject.data.database

import androidx.room.TypeConverter
import com.example.capstoneproject.data.models.db.SourceData

class Converters {

    @TypeConverter
    fun fromSource(source: SourceData): String? {
        return source.name
    }

    @TypeConverter
    fun toSource(name: String): SourceData {
        return SourceData(name, name)
    }

}