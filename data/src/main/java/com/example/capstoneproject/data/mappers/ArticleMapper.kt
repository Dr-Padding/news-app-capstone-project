package com.example.capstoneproject.data.mappers

import com.example.capstoneproject.data.models.db.FavoriteArticle
import javax.inject.Inject
import com.example.capstoneproject.data.models.db.ArticleData as DataArticle
import com.example.capstoneproject.domain.models.ArticleEntity as DomainArticle


class ArticleMapper @Inject constructor() {

    fun mapFromData(dataArticle: DataArticle): DomainArticle {
        return DomainArticle(
            id = dataArticle.id ?: 0,
            description = dataArticle.description.orEmpty(),
            title = dataArticle.title.orEmpty(),
            url = dataArticle.url.orEmpty(),
            urlToImage = dataArticle.urlToImage.orEmpty()
        )
    }

    fun mapToData(domainArticle: DomainArticle): DataArticle {
        return DataArticle(
            id = domainArticle.id,
            description = domainArticle.description,
            title = domainArticle.title,
            url = domainArticle.url,
            urlToImage = domainArticle.urlToImage
        )
    }

    fun mapToFavoriteArticleData(domainArticle: DomainArticle): FavoriteArticle {
        return FavoriteArticle(
            id = domainArticle.id,
            description = domainArticle.description,
            title = domainArticle.title,
            url = domainArticle.url,
            urlToImage = domainArticle.urlToImage
        )
    }

}
