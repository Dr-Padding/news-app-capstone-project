package com.example.capstoneproject.data.models.db

data class SourceData(
    val id: Any?,
    val name: String?
)