package com.example.capstoneproject.data.models.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "articles"
)

data class ArticleData(
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null,
    val description: String?,
    val title: String?,
    val url: String?,
    val urlToImage: String?
)