package com.example.capstoneproject.data.api


import com.example.capstoneproject.data.models.remote.NewsResponseData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsAPI {
    @GET("v2/top-headlines")
    suspend fun getBreakingNews(
        @Query("country")
        countryCode: String = "us",
        @Query("page")
        pageNumber: Int = 1,
        @Query("apiKey")
        apiKey: String = API_KEY
    ): Response<NewsResponseData>

    companion object {
        const val API_KEY = "6005719c40d94feea9543048819134be"
    }
}