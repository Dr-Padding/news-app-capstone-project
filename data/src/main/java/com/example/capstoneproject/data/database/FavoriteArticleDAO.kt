package com.example.capstoneproject.data.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.capstoneproject.data.models.db.FavoriteArticle
import kotlinx.coroutines.flow.Flow


@Dao
interface FavoriteArticleDAO {
    @Query("SELECT * FROM favorite_articles")
    fun getSavedArticles(): Flow<List<FavoriteArticle>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertArticle(article: FavoriteArticle)

    @Delete
    suspend fun deleteArticle(article: FavoriteArticle)
}