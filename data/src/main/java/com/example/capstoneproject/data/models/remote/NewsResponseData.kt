package com.example.capstoneproject.data.models.remote

import com.example.capstoneproject.data.models.db.ArticleData

data class NewsResponseData(
    val articles: List<ArticleData>,
    val status: String,
    val totalResults: Int
)